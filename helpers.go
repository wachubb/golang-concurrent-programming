package main

import (
	"fmt"
)

func readChannel(primeNumbersChannel chan int, totalWorkers int) {
	completedWorkerCount := 0
	totalPrimes := []int{}
	fmt.Println("here")

	for item := range primeNumbersChannel {
		fmt.Println(item)
		if item == -1 {
			completedWorkerCount++
			continue
		}

		totalPrimes = append(totalPrimes, item)
	}

	fmt.Println(totalPrimes)

	close(primeNumbersChannel)
}

func createRange(startingInt int, endingInt int) []int {
	intRange := []int{}

	for i := startingInt; i <= endingInt; i++ {
		intRange = append(intRange, i)
	}

	return intRange
}
