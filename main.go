package main

import (
	"fmt"
	"math"
	"sort"
	"sync"
)

func main() {
	// Get the ranges
	ranges := [][]int{{1, 50}, {51, 100}, {20, 200}, {201, 500}, {501, 1000}}

	primeNumbersChannel := make(chan int, len(ranges))

	go func() {
		wg := sync.WaitGroup{}

		// Trigger a goroutine for each range
		for n := 0; n < len(ranges); n++ {
			wg.Add(1)
			routineId := n + 1
			currentRange := createRange(ranges[n][0], ranges[n][1])

			go func() {
				defer wg.Done()

				fmt.Println("Goroutine " + fmt.Sprint(routineId) + " started")
				for _, currNum := range currentRange {
					if isPrime(currNum) {
						primeNumbersChannel <- currNum
					}
				}

				fmt.Println("Goroutine " + fmt.Sprint(routineId) + " finished")
			}()
		}

		wg.Wait()
		close(primeNumbersChannel)
	}()

	totalPrimes := []int{}

	// Set up the channel reader
	// 	for completedWorkerCount < len(ranges) {
	for primeNum := range primeNumbersChannel {
		// if primeNum == -1 {
		// 	// completedWorkerCount++
		// 	continue
		// }

		totalPrimes = append(totalPrimes, primeNum)
	}

	// Order the numbers and deduplicate
	sort.Ints(totalPrimes)
	totalPrimes = deduplicateSortedSlice(totalPrimes)

	// print out the final list of numbers
	fmt.Println("Total number of primes: " + fmt.Sprint(len(totalPrimes)))
	fmt.Println(totalPrimes)
}

func isPrime(numToCheck int) bool {
	if numToCheck < 2 {
		return false
	} else if numToCheck == 2 {
		return true
	}

	sqRt := int(math.Sqrt(float64(numToCheck)))

	for i := 2; i <= sqRt; i++ {
		if numToCheck%i == 0 {
			return false
		}
	}

	return true
}

func deduplicateSortedSlice(sortedInputSlice []int) []int {
	returnSlice := []int{sortedInputSlice[0]}

	for i := 1; i < len(sortedInputSlice); i++ {
		if sortedInputSlice[i] != sortedInputSlice[i-1] {
			returnSlice = append(returnSlice, sortedInputSlice[i])
		}
	}

	return returnSlice
}
